package model

type Config struct {
	Port             string
	DatabaseHost     string
	DatabaseUser     string
	DatabasePassword string
	DatabaseName     string
}
