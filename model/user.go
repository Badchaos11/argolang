package model

import "time"

type NewUser struct {
	Username string `json:"username" faker:"username"`
	Password string `json:"password" faker:"password"`
	Name     string `json:"name" faker:"first_name"`
	Surname  string `json:"surname" faker:"last_name"`
	Role     string `json:"role"`
}

type LoginRequest struct {
	Login    string `json:"username"`
	Password string `json:"password"`
}

type ResponseUser struct {
	Username string    `json:"username"`
	Name     string    `json:"name"`
	Surname  string    `json:"surname"`
	Role     string    `json:"role"`
	Created  time.Time `json:"created"`
}

type ChangeRoleRequest struct {
	UserID  int64  `json:"user_id"`
	NewRole string `json:"new_role"`
}

type User struct {
	ID       int64     `json:"id"`
	Username string    `json:"username"`
	Password string    `json:"password"`
	Name     string    `json:"name"`
	Surname  string    `json:"surname"`
	Role     string    `json:"role"`
	Created  time.Time `json:"created"`
	Updated  time.Time `json:"updated"`
}
