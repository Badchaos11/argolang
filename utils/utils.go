package utils

import (
	"net/http"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/middlevare"
)

func CheckDuplicateUserError(err error) bool {
	return strings.Contains(err.Error(), "duplicate key value violates unique constraint")
}

func CheckErrNoRows(err error) bool {
	return strings.Contains(err.Error(), "no rows")
}

func GetUsernameFromCookies(r *http.Request) string {
	tokenCookie, err := r.Cookie("token")
	if err != nil {
		logrus.Errorf("error getting token: %v", err)
		return ""
	}
	if tokenCookie == nil {
		logrus.Errorf("token cookie is nil")
		return ""
	}
	tokenStr := tokenCookie.Value

	authJwt, err := middlevare.ParseToken(tokenStr, os.Getenv("JWT_SECRET"))
	if err != nil {
		logrus.Errorf("error parsing jwt token error: %v", err)
		return ""
	}

	if authJwt == nil {
		logrus.Errorf("token is nil")
		return ""
	}

	return authJwt.Username
}
