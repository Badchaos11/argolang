package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/middlevare"
	"gitlab.com/Badchaos11/argolang/model"
	"gitlab.com/Badchaos11/argolang/utils"
)

func (s *Service) Register(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Get Register request. Started registration process")

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Errorf("Error reading body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения тела запроса, попробуйте ещё раз."))
		return
	}

	var req model.NewUser

	err = jsoniter.Unmarshal(body, &req)
	if err != nil {
		logrus.Errorf("Error unmarshalling request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения данных, попробуйте ещё раз."))
		return
	}
	ctx := context.Background()
	newUserId, err := s.repo.CreateUser(ctx, req)
	if err != nil {
		logrus.Errorf("Error creating new user error: %v", err)
		if utils.CheckDuplicateUserError(err) {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Пользователь с таким логином уже существует."))
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка сохранения пользователя, попробуйте ещё раз."))
		return
	}
	logrus.Info("User successfully registered with id: ", newUserId)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Пользователь с id %v успешно создан.", newUserId)))
}

func (s *Service) Login(w http.ResponseWriter, r *http.Request) {

	logrus.Info("Get Login request. Started process")

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Errorf("Error reading body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения тела запроса, попробуйте ещё раз."))
		return
	}

	var req model.LoginRequest

	err = jsoniter.Unmarshal(body, &req)
	if err != nil {
		logrus.Errorf("Error unmarshalling request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения тела запроса, попробуйте позже."))
		return
	}
	ctx := context.Background()

	role, err := s.repo.GetUserForLogin(ctx, req)
	if err != nil {
		logrus.Error("error getting user for login: ", err)
		if utils.CheckErrNoRows(err) {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Пользователь не зарегистрирован."))
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка авторизации, попробуйте снова."))
		return
	}

	token, err := middlevare.NewToken(ctx, req.Login, role)
	if err != nil {
		logrus.Errorf("error generate jwt for user ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка авторизации, попробуйте ещё раз."))
		return
	}

	cookie := &http.Cookie{
		Name:    "token",
		Value:   token,
		Expires: time.Now().Add(time.Hour * 1),
	}

	http.SetCookie(w, cookie)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Выполнен вход в аккаунт."))
}

// Функционал зарегистрированного пользователя
func (s *Service) Logout(w http.ResponseWriter, r *http.Request) {
	сookie := &http.Cookie{
		Name:   "token",
		MaxAge: -1,
	}
	http.SetCookie(w, сookie)

	w.Write([]byte("Выполнен выход из аккаунта."))
}

func (s *Service) GetUserByID(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Got GetUserByID request. Starting process")
	userStringId := r.URL.Query().Get("user_id")
	userId, err := strconv.ParseInt(userStringId, 10, 64)
	if err != nil {
		logrus.Errorf("error parsing user_id: ", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Введите корректный id пользователя."))
		return
	}

	ctx := context.Background()
	user, err := s.repo.GetUserByID(ctx, userId)
	if err != nil {
		logrus.Errorf("error getting user from db error: ", err)
		if utils.CheckErrNoRows(err) {
			w.WriteHeader(http.StatusNoContent)
			w.Write([]byte("Пользователь с таким id не зарегистрированю"))
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка получения данных пользователя, попробуйте позже."))
		return
	}

	body, err := json.Marshal(user)
	if err != nil {
		logrus.Errorf("error marshalling user into json: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка получения пользователя, попробуйте позже."))
		return
	}

	logrus.Info("Succesfully got user")
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (s *Service) ChangePassword(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Got ChangePassword request. Starting process")

	type request struct {
		NewPassword string `json:"new_password"`
	}
	var req request

	username := utils.GetUsernameFromCookies(r)
	if username == "" {
		logrus.Error("error get username from request")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка обновления пароля, попробуйте ещё раз."))
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Errorf("error reading request body: ", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Неправильно заданы пароль и id пользователя, попробуйте ещё раз."))
		return
	}

	if err := jsoniter.Unmarshal(body, &req); err != nil {
		logrus.Errorf("error unmarshaling request body: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения тела запроса, попробуйте позже."))
		return
	}

	ctx := context.Background()
	err = s.repo.ChangePassword(ctx, req.NewPassword, username)
	if err != nil {
		logrus.Errorf("error changing password in db ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка удаления пользователя, попробуйте ещё раз."))
	}

	logrus.Info("Password changed succesfully")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Пароль успешно изменён."))
}
