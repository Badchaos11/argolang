package service

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/middlevare"
	"gitlab.com/Badchaos11/argolang/model"
	"gitlab.com/Badchaos11/argolang/repository"
)

type Service struct {
	port string
	repo repository.IRepository
}

func NewService(conf *model.Config) (*Service, error) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%v sslmode=disable", conf.DatabaseHost, conf.DatabaseUser, conf.DatabasePassword, conf.DatabaseName)

	r, err := repository.NewRepository(dsn)
	if err != nil {
		logrus.Errorf("error creating repository: %v", err)
		return nil, err
	}

	return &Service{
		repo: r,
		port: conf.Port,
	}, nil
}

func (s *Service) Run() {

	router := mux.NewRouter()

	router.HandleFunc("/register", s.Register).Methods("POST")
	router.HandleFunc("/login", s.Login).Methods("POST")

	commonRouter := router.NewRoute().Subrouter()
	commonRouter.Use(middlevare.AuthMiddleware)
	commonRouter.HandleFunc("/change_password", s.ChangePassword).Methods("POST")
	commonRouter.HandleFunc("/logout", s.Logout).Methods("GET")
	commonRouter.HandleFunc("/get_user", s.GetUserByID).Methods("GET")

	adminRouter := router.NewRoute().Subrouter()
	adminRouter.Use(middlevare.AdminAuthMiddleware)
	adminRouter.HandleFunc("/delete_user", s.DeleteUser).Methods("GET")
	adminRouter.HandleFunc("/change_password_by_id", s.ChangePasswordByID).Methods("POST")
	adminRouter.HandleFunc("/change_role", s.ChangeUserRole).Methods("POST")
	adminRouter.HandleFunc("/get_full_user_data", s.GetFullUserData).Methods("GET")

	server := &http.Server{
		Addr:         fmt.Sprintf(":%v", s.port), // Порт сервера
		Handler:      router,                     // Хэндлеры
		ReadTimeout:  5 * time.Second,            // Таймаут запроса клиента
		WriteTimeout: 10 * time.Second,           // Таймаут ответа клиенту
		IdleTimeout:  120 * time.Second,          // Таймаут соединения в простое
	}

	go func() {
		logrus.Infof("starting server on port %v", s.port)

		err := server.ListenAndServe()
		if err != nil {
			logrus.Errorf("error starting server %v", err)
			os.Exit(1)
		}
	}()

	// Отключение
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	sig := <-c
	logrus.Infof("Got signal: %v", sig)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	server.Shutdown(ctx)
}

func init() {
	f, err := os.OpenFile("./logs/log.txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println("Failed to create logfile" + "log.txt")
		panic(err)
	}

	mw := io.MultiWriter(f, os.Stdout)

	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		ForceColors:     true,
		DisableColors:   false,
		FullTimestamp:   true,
	})

	logrus.SetOutput(mw)

	logrus.SetLevel(logrus.DebugLevel)
}
