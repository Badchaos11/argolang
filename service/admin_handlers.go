package service

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"strconv"

	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/model"
)

func (s *Service) GetFullUserData(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Got GetFullUserByID request. Starting process")
	userStringId := r.URL.Query().Get("user_id")
	userId, err := strconv.ParseInt(userStringId, 10, 64)
	if err != nil {
		logrus.Errorf("error parsing user_id: ", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Введите корректный id пользователя."))
		return
	}

	ctx := context.Background()
	user, err := s.repo.GetFullUserDataByID(ctx, userId)
	if err != nil {
		logrus.Errorf("error getting user from db error: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка получения данных пользователя, попробуйте позже."))
		return
	}

	body, err := json.Marshal(user)
	if err != nil {
		logrus.Errorf("error marshalling user into json: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка получения пользователя, попробуйте позже."))
		return
	}

	logrus.Info("Succesfully got user")
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (s *Service) DeleteUser(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Got DeleteUser request. Starting process")
	userStringId := r.URL.Query().Get("user_id")
	userId, err := strconv.ParseInt(userStringId, 10, 64)
	if err != nil {
		logrus.Errorf("error parsing user_id: ", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Введите корректный id пользователя."))
		return
	}

	ctx := context.Background()
	err = s.repo.DeleteUser(ctx, userId)
	if err != nil {
		logrus.Errorf("error deleting user: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка удаления пользователя, попробуйте ещё раз."))
		return
	}

	logrus.Info("User deleted succesfully")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Пользователь успешно удален."))
}

func (s Service) ChangeUserRole(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Got ChangeUserRole request. Starting process")

	var req model.ChangeRoleRequest

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Errorf("error reading request body: ", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Неправильно заданы роль и id пользователя, попробуйте ещё раз."))
		return
	}

	if err := jsoniter.Unmarshal(body, &req); err != nil {
		logrus.Errorf("error unmarshaling request body: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения тела запроса, попробуйте позже."))
		return
	}

	ctx := context.Background()
	err = s.repo.ChangeUserRole(ctx, req.UserID, req.NewRole)
	if err != nil {
		logrus.Errorf("error changing password in db ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка изменения роли пользователя, попробуйте ещё раз."))
		return
	}

	logrus.Info("Role changed succesfully")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Роль успешно изменена."))
}

func (s *Service) ChangePasswordByID(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Got ChangePasswordByID request. Starting process")

	type request struct {
		UserID      int64  `json:"user_id"`
		NewPassword string `json:"new_password"`
	}
	var req request

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Errorf("error reading request body: ", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Неправильно заданы пароль и id пользователя, попробуйте ещё раз."))
		return
	}

	if err := jsoniter.Unmarshal(body, &req); err != nil {
		logrus.Errorf("error unmarshaling request body: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка чтения тела запроса, попробуйте позже."))
		return
	}

	ctx := context.Background()
	err = s.repo.ChangePasswordByID(ctx, req.UserID, req.NewPassword)
	if err != nil {
		logrus.Errorf("error changing password in db ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Ошибка удаления пользователя, попробуйте ещё раз."))
	}

	logrus.Info("Password changed succesfully")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Пароль успешно изменён."))
}
