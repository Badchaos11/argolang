package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/config"
	"gitlab.com/Badchaos11/argolang/service"
)

func main() {
	conf, err := config.LoadConfig()
	if err != nil {
		logrus.Fatal(err)
	}

	app, err := service.NewService(conf)
	if err != nil {
		logrus.Fatal(err)
	}

	app.Run()
}
