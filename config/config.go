package config

import (
	"flag"
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/model"
)

func LoadConfig() (*model.Config, error) {
	conf, envFile := parseFlags()

	err := godotenv.Load(fmt.Sprintf("./configs/%v.env", envFile))
	if err != nil {
		logrus.Errorf("failed to load config: %v", err)
		return nil, err
	}

	if conf.Port == "" {
		conf.Port = os.Getenv("PORT")
	}
	if conf.DatabaseHost == "" {
		conf.DatabaseHost = os.Getenv("DATABASE_HOST")
	}
	if conf.DatabaseUser == "" {
		conf.DatabaseUser = os.Getenv("DATABASE_USER")
	}
	if conf.DatabasePassword == "" {
		conf.DatabasePassword = os.Getenv("DATABASE_PASSWORD")
	}
	if conf.DatabaseName == "" {
		conf.DatabaseName = os.Getenv("DATABASE_NAME")
	}

	return conf, nil
}

func parseFlags() (*model.Config, string) {
	portPtr := flag.String("port", "", "port for server listening")
	dbHostPtr := flag.String("dbhost", "", "database host")
	dbUserPtr := flag.String("dbuser", "", "database user")
	dbPassPtr := flag.String("dbpasswd", "", "database user password")
	dbNamePtr := flag.String("dbname", "", "database name")
	envFilePtr := flag.String("envfile", "", "database name")
	flag.Parse()
	var conf model.Config

	conf.Port = *portPtr
	conf.DatabaseHost = *dbHostPtr
	conf.DatabaseUser = *dbUserPtr
	conf.DatabasePassword = *dbPassPtr
	conf.DatabaseName = *dbNamePtr

	return &conf, *envFilePtr
}
