--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: public; Owner: argolanguser
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    name text NOT NULL,
    surname text NOT NULL,
    role text NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO argolanguser;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: argolanguser
--

ALTER TABLE public.users ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: argolanguser
--

INSERT INTO public.users VALUES (1, 'DnnxLQp', 'fgCPOSvcPdDVVoejMDCMHUVLuhXSAbgZPlrREdCPOCdZKqYVsP', 'Ernestine', 'Hodkiewicz', 'administrator', '2023-01-16 08:55:15.50901', '2023-01-16 08:55:15.50901');
INSERT INTO public.users VALUES (2, 'ASFhEsR', 'kBfSvfkpcNBwLUUuulIJFIvaBAZLveqavqFwEMVioQQvEotFuA', 'Emilie', 'Kuhlman', 'administrator', '2023-01-16 08:55:15.510385', '2023-01-16 08:55:15.510385');
INSERT INTO public.users VALUES (3, 'WQZfaYv', 'KlfNfpbhAxUwolvIQcbRYoKBsSHtnLPxyuQBIIYOsSltcgNeEH', 'Jensen', 'Carroll', 'administrator', '2023-01-16 08:55:15.510689', '2023-01-16 08:55:15.510689');
INSERT INTO public.users VALUES (4, 'oBUYCvg', 'RNgxOWiJmeWNaKYRNpeXLnjJaWZuvOrZdrBblGpjPACJeSXrSY', 'Jonatan', 'Hermiston', 'administrator', '2023-01-16 08:55:15.510961', '2023-01-16 08:55:15.510961');
INSERT INTO public.users VALUES (5, 'toeyWPw', 'ljucwGgYukYwFJKHKdhJoKjRfYdwXfsmdsINsKOQMCkdhBxmCH', 'Dejon', 'Cartwright', 'administrator', '2023-01-16 08:55:15.511195', '2023-01-16 08:55:15.511195');
INSERT INTO public.users VALUES (6, 'mblZZSH', 'xItNjSMDgqsEnCUPiCjvxxeevAJecvigYWfSJnLuRabiAypyAV', 'Kaela', 'Kassulke', 'administrator', '2023-01-16 08:55:15.511418', '2023-01-16 08:55:15.511418');
INSERT INTO public.users VALUES (7, 'BTvNlTN', 'elSDAMVkaHiDsFPklNwywvfCIFJDFUiZtndeiRQZIrkZNOyHfJ', 'Hulda', 'Reilly', 'administrator', '2023-01-16 08:55:15.511636', '2023-01-16 08:55:15.511636');
INSERT INTO public.users VALUES (8, 'pQmTHNG', 'EoBSVWqgZPWSkjqRMkgUSMoLYWRFvCFrFbjomoZCjieDDMHOsj', 'Clovis', 'Hickle', 'administrator', '2023-01-16 08:55:15.511845', '2023-01-16 08:55:15.511845');
INSERT INTO public.users VALUES (9, 'Udringr', 'QJruirZSEFCbgGcpodYCQnHNypvaQSAdcsyMSQjLQCvcHBTQXO', 'Kole', 'Huels', 'administrator', '2023-01-16 08:55:15.512056', '2023-01-16 08:55:15.512056');
INSERT INTO public.users VALUES (10, 'tjDOlPi', 'JkZxmCAKprlevxtFwhIYCBMiNgbgFoGOyQNJvQhLQcKqjryYrp', 'Katelynn', 'Satterfield', 'administrator', '2023-01-16 08:55:15.512266', '2023-01-16 08:55:15.512266');
INSERT INTO public.users VALUES (11, 'hiquHSu', 'hKDHFsnUIdfxIhYpqTIXskMbAvrfUmduPxrGLnpTolScPCmEek', 'Allie', 'Koelpin', 'user', '2023-01-16 08:55:15.512456', '2023-01-16 08:55:15.512456');
INSERT INTO public.users VALUES (12, 'lGOYfVo', 'GsBckZIkwLLIsDtVkIXlyxPprSgArDTfqsZVdtVkHrxSoZhQvu', 'Edna', 'Durgan', 'user', '2023-01-16 08:55:15.512648', '2023-01-16 08:55:15.512648');
INSERT INTO public.users VALUES (13, 'LvfwvgR', 'cstqrctboHkXWQkUuQCFeCPWuhctDaIlypYGUKnsLnBTWUjDbs', 'Orland', 'Johnson', 'user', '2023-01-16 08:55:15.512853', '2023-01-16 08:55:15.512853');
INSERT INTO public.users VALUES (14, 'KEKWhaL', 'QcwkHcmkoiDYKayGXDGKefrLjPGVeUmWeFpvUIEgQYfCvvCsBX', 'Nico', 'Hirthe', 'user', '2023-01-16 08:55:15.513051', '2023-01-16 08:55:15.513051');
INSERT INTO public.users VALUES (15, 'ZnoQgXi', 'mnytdeqGRxCpEGSxAFmAbdrbSwxNNOEfGyBFDfYtxndwJhtKfG', 'Kathleen', 'Weissnat', 'user', '2023-01-16 08:55:15.513248', '2023-01-16 08:55:15.513248');
INSERT INTO public.users VALUES (16, 'fGqiAGH', 'eEEnPDgvrQGvmOCpGuSUWNxqHwaaTaIknTibvqOSIDvKBkwOxW', 'Joe', 'Mertz', 'user', '2023-01-16 08:55:15.513473', '2023-01-16 08:55:15.513473');
INSERT INTO public.users VALUES (17, 'GBldtjK', 'ZnfrTItwfudlKHcKBAEdvtvLDMPGpcJRFqUgqudlCeLUZlZkHg', 'Jacquelyn', 'Windler', 'user', '2023-01-16 08:55:15.513673', '2023-01-16 08:55:15.513673');
INSERT INTO public.users VALUES (18, 'qNXYVpA', 'bPLmxPPCMUsQtGpiPDYVbqGNUrVPARNRXAXVhSYHVHPZbPVnrk', 'Melody', 'Reinger', 'user', '2023-01-16 08:55:15.51387', '2023-01-16 08:55:15.51387');
INSERT INTO public.users VALUES (19, 'aCTUQPM', 'xrygSsLPNZrklseHVgmySGQjBKojYJRvqxcLmVyEYmPDFToQEX', 'Myles', 'Fisher', 'user', '2023-01-16 08:55:15.514076', '2023-01-16 08:55:15.514076');
INSERT INTO public.users VALUES (20, 'qkPJBkB', 'IMumIvUDSihqDMhdPCjNgMqebAAOfPRynjYMWfCArALEsHTpOK', 'Ashlynn', 'O''Keefe', 'user', '2023-01-16 08:55:15.514265', '2023-01-16 08:55:15.514265');
INSERT INTO public.users VALUES (21, 'ljAZbkS', 'fKfTiRhTlcnsKVyOVSxIWCSHHWXSpoJeDJDhWfqdXYXNqZHVtv', 'Allison', 'Smitham', 'user', '2023-01-16 08:55:15.514454', '2023-01-16 08:55:15.514454');
INSERT INTO public.users VALUES (22, 'SpDRQCs', 'JhWyZGooHVWgMVgHRykuMejeGFyDEuqBKUtXlVqtMvYlHJfYyq', 'Gene', 'Goodwin', 'user', '2023-01-16 08:55:15.51466', '2023-01-16 08:55:15.51466');
INSERT INTO public.users VALUES (23, 'kNbVOAd', 'NFRbViUqXXIaONJlixXvMBeNavcXfDEVGmhlVVdskrImCTeXYn', 'Sarai', 'Johnston', 'user', '2023-01-16 08:55:15.51485', '2023-01-16 08:55:15.51485');
INSERT INTO public.users VALUES (25, 'kBHkwOe', 'caEuBVcuNXcAXnnOKWcKCkHFKKqwNFhJObaMnyeojTnBsHvnUv', 'Iva', 'Russel', 'user', '2023-01-16 08:55:15.515236', '2023-01-16 08:55:15.515236');
INSERT INTO public.users VALUES (26, 'cydwrsG', 'ahbYHXanlvfpbrVcaLkpDPDUiQCeDopricicIlOtpybZtFKqkw', 'Rene', 'Hegmann', 'user', '2023-01-16 08:55:15.51543', '2023-01-16 08:55:15.51543');
INSERT INTO public.users VALUES (27, 'hBMMpUA', 'ElovRwlZWApLGqPoPdHiTVsbSrTxEHgpkFYPkjQGloAVZqaNoL', 'Ludie', 'Brakus', 'user', '2023-01-16 08:55:15.51562', '2023-01-16 08:55:15.51562');
INSERT INTO public.users VALUES (28, 'eUvjewc', 'uTkeFNLmSHjwgcbEyhSdNRBwPecORslnHtXhjTqOwmrMYLkdNP', 'Litzy', 'Kuhn', 'user', '2023-01-16 08:55:15.515821', '2023-01-16 08:55:15.515821');
INSERT INTO public.users VALUES (29, 'odumRoc', 'ZspsDRemCdjhmFIZatsMMADxfxQUVZyOrpZcyHmiYguSQoExGK', 'Albertha', 'Braun', 'user', '2023-01-16 08:55:15.51601', '2023-01-16 08:55:15.51601');
INSERT INTO public.users VALUES (30, 'seUcHWW', 'OYtapBicujguKdWwefTJGgMdXVUdMnOFlZqGOWYvBtvNubLBWo', 'Rogers', 'Prosacco', 'user', '2023-01-16 08:55:15.516206', '2023-01-16 08:55:15.516206');
INSERT INTO public.users VALUES (31, 'NZEgvSj', 'JjhHgwxbdoYokrWUJWeNlXVgFVQNYdUWsAyyLcgGnSxCoxDALu', 'Jane', 'Leffler', 'user', '2023-01-16 08:55:15.516414', '2023-01-16 08:55:15.516414');
INSERT INTO public.users VALUES (32, 'gwGnrSO', 'OpmaoiJmVckiSLGHTbWQKbVLfhaYplHsPoTkCdVImLXVdgxfOv', 'Gladys', 'Schumm', 'user', '2023-01-16 08:55:15.516624', '2023-01-16 08:55:15.516624');
INSERT INTO public.users VALUES (33, 'rdwlJfQ', 'RRUlvMXIJyZcFSJgMCIxMXIiFLAvhjCSyLMrdWceCGYDgvFZta', 'Keeley', 'Lesch', 'user', '2023-01-16 08:55:15.516814', '2023-01-16 08:55:15.516814');
INSERT INTO public.users VALUES (34, 'FCfblkp', 'ZMwTCkVftvRAEuJPGaSIoGgNSqfsgfxDZLcfXjdAkGWgXQkPgP', 'Jarrell', 'Ritchie', 'user', '2023-01-16 08:55:15.51701', '2023-01-16 08:55:15.51701');
INSERT INTO public.users VALUES (35, 'IOnCIaX', 'kZOIEgrBaqSnbQLSTdEnjOybfmcrwOBRFrbVlcaSoBhGphVdww', 'Elsie', 'Watsica', 'user', '2023-01-16 08:55:15.5172', '2023-01-16 08:55:15.5172');
INSERT INTO public.users VALUES (36, 'bhwQjaN', 'xIPAejaAJCmgyOhDVBXXgHFcdxcrALspbFdsGSnLiDcPATVTSn', 'Betty', 'Reinger', 'user', '2023-01-16 08:55:15.517383', '2023-01-16 08:55:15.517383');
INSERT INTO public.users VALUES (37, 'SEHaHoc', 'vqkMfjvaRtqhyhOhcocYOAPWmhqBtyCDpxlVRpCpKlNxbXCveP', 'Kurt', 'Dibbert', 'user', '2023-01-16 08:55:15.517671', '2023-01-16 08:55:15.517671');
INSERT INTO public.users VALUES (38, 'ZZRrXAe', 'UioAcwYVonnGngPAjlZCYeDlswPSZNeFGsafivfDMvLXBZuhKO', 'Lorine', 'Heathcote', 'user', '2023-01-16 08:55:15.518242', '2023-01-16 08:55:15.518242');
INSERT INTO public.users VALUES (39, 'sniEHHI', 'wFaQdHuCLTykhuogkvptvAhBltAtcIWSPGgFmLauDgxEAYkNZW', 'Kaden', 'Schoen', 'user', '2023-01-16 08:55:15.518597', '2023-01-16 08:55:15.518597');
INSERT INTO public.users VALUES (40, 'KiPNNEt', 'bSAkLXKqsVxpRaSDmxhLyEOOwRROIyrTOWIQtJwjuoTDwWgFlN', 'Coleman', 'Batz', 'user', '2023-01-16 08:55:15.518838', '2023-01-16 08:55:15.518838');
INSERT INTO public.users VALUES (41, 'QhTfpKu', 'LUxRCGAoTMUrmyYrSlLLjxXdfCMdYSIewdlTPsAWPfbuqIuOWk', 'Christophe', 'Carter', 'user', '2023-01-16 08:55:15.519049', '2023-01-16 08:55:15.519049');
INSERT INTO public.users VALUES (42, 'sVvsKBI', 'KishfAYcCpuXashIdlcIurSyyLSbdqjgiVCwBtWpcOvvhwPiEh', 'Zelda', 'Wisoky', 'user', '2023-01-16 08:55:15.519249', '2023-01-16 08:55:15.519249');
INSERT INTO public.users VALUES (43, 'MKnXtyr', 'efHPPtUwmVRoLcbhAGbwqMtwpjRUTsuqsleFbQdfcmoycMgipe', 'Lia', 'Becker', 'user', '2023-01-16 08:55:15.519447', '2023-01-16 08:55:15.519447');
INSERT INTO public.users VALUES (44, 'ApjunSC', 'ODwTyKEYPnMMYELHtYooYVFbuhDMfXRASufhWNSIopWmIPLcem', 'Darrion', 'Bernier', 'user', '2023-01-16 08:55:15.519641', '2023-01-16 08:55:15.519641');
INSERT INTO public.users VALUES (45, 'xNFKItB', 'ySwWfnhWMPiBABoBrkGWnvMlPWBQtYRdLPuRcffFXvDCqPyFao', 'Oscar', 'Yundt', 'user', '2023-01-16 08:55:15.519852', '2023-01-16 08:55:15.519852');
INSERT INTO public.users VALUES (46, 'dkuqIGj', 'gvsmAbuprWoAaQbNByfXZvqbdEETXgjCGsQFsKHrUhvJRSVJDv', 'Coby', 'Fritsch', 'user', '2023-01-16 08:55:15.520045', '2023-01-16 08:55:15.520045');
INSERT INTO public.users VALUES (47, 'fxIyAPJ', 'cOCtKvcbxgwTqmORWlRCQIeqUGEeIxsoKGbKhFWJDdcoWnQkhK', 'Elwyn', 'Wolff', 'user', '2023-01-16 08:55:15.520239', '2023-01-16 08:55:15.520239');
INSERT INTO public.users VALUES (48, 'YfYTnqa', 'cFREQgOSoLLJJiMmnactjrZFbVjTsxFjhPSPJwFwyBuuxCHpgL', 'Sylvan', 'O''Kon', 'user', '2023-01-16 08:55:15.520742', '2023-01-16 08:55:15.520742');
INSERT INTO public.users VALUES (49, 'ixJsgVc', 'LkRslYgLTwOHjnBDTdBHBFrgtssmRKSaQpSOLFdKKfWkhmZaNM', 'Ramon', 'Roberts', 'user', '2023-01-16 08:55:15.520957', '2023-01-16 08:55:15.520957');
INSERT INTO public.users VALUES (50, 'UUgeSUJ', 'XlqLlHDcZyLANhTYwfHAoSYAMrgWFCTnRxDlGWeGFCtoGiTojd', 'Brianne', 'Adams', 'user', '2023-01-16 08:55:15.521166', '2023-01-16 08:55:15.521166');
INSERT INTO public.users VALUES (51, 'Jymbjmc', 'CTJTMqgOJWkwhySYTZtPXnECYlwUJvjhViJvSpvDUcPXLwhLkR', 'Camille', 'Orn', 'user', '2023-01-16 08:55:15.521365', '2023-01-16 08:55:15.521365');
INSERT INTO public.users VALUES (52, 'SuKDvQC', 'ZUjYdqWNvRGsrmlNPInnQMpOilsQeuRyYdRqfkeTCYyUNQciWA', 'Norbert', 'Cole', 'user', '2023-01-16 08:55:15.521566', '2023-01-16 08:55:15.521566');
INSERT INTO public.users VALUES (53, 'jdtllFg', 'mVloAJmoewRILITlQjnfJkaZfFPRWSsDAXoXLtoErdncyGOcpv', 'Granville', 'Daugherty', 'user', '2023-01-16 08:55:15.521754', '2023-01-16 08:55:15.521754');
INSERT INTO public.users VALUES (54, 'wPJtthK', 'nNpfTRpvTQmHoQeysxTScXdkZFQXBYmjlUfKRZqBhYRUBDkQIJ', 'Jimmy', 'West', 'user', '2023-01-16 08:55:15.521955', '2023-01-16 08:55:15.521955');
INSERT INTO public.users VALUES (55, 'LDwMxig', 'raAMskDsktYJCpJmGNttjsxlNeHsinSdqVUuIXwwnCdnmfCGFK', 'Darlene', 'Gusikowski', 'user', '2023-01-16 08:55:15.522146', '2023-01-16 08:55:15.522146');
INSERT INTO public.users VALUES (56, 'jcEdUSr', 'BXcSHInqlftcJxfyDJgorYgNoOLxDcOIQCrImYADTcpgvbliPF', 'Diego', 'Harber', 'user', '2023-01-16 08:55:15.522333', '2023-01-16 08:55:15.522333');
INSERT INTO public.users VALUES (57, 'jGsqZNG', 'fcyrXGTLkpCavXBYnsEXtROBWtaagmNyXxAEBsImntaNseIZfZ', 'Jessika', 'Fahey', 'user', '2023-01-16 08:55:15.522597', '2023-01-16 08:55:15.522597');
INSERT INTO public.users VALUES (58, 'jxrHkty', 'aqeWqOYNSyLquyMhpBvnTdXkmWliQJbeOedvAOWWCWNZftANmc', 'Carson', 'Abshire', 'user', '2023-01-16 08:55:15.522877', '2023-01-16 08:55:15.522877');
INSERT INTO public.users VALUES (59, 'RuwKZDJ', 'YnEJOhOsPDrxtyFLnEjEXigTbduKqdolJEEJnciSOIkUWmovLm', 'Lilian', 'Adams', 'user', '2023-01-16 08:55:15.523163', '2023-01-16 08:55:15.523163');
INSERT INTO public.users VALUES (60, 'cxATBif', 'AfRvQOfcQplJLUbGoByusxDlbyuMfOJPEslxWfAGlEnxaMOlXa', 'Thelma', 'Kessler', 'user', '2023-01-16 08:55:15.523367', '2023-01-16 08:55:15.523367');
INSERT INTO public.users VALUES (61, 'KPIHpyt', 'bqWxKLyJfQStphhVMsXnRAcryIcTvqelgcZQsLYSCpFDpllRGT', 'Robert', 'Bailey', 'user', '2023-01-16 08:55:15.52356', '2023-01-16 08:55:15.52356');
INSERT INTO public.users VALUES (62, 'NVmHIPc', 'vunSbnEqooFyJFxfTjpdAhpVxIewXXhcxnAMsrypxgZPUoVKSy', 'Alvera', 'Metz', 'user', '2023-01-16 08:55:15.524104', '2023-01-16 08:55:15.524104');
INSERT INTO public.users VALUES (63, 'WSWoEyl', 'cPVHxBOqafueACZdFNDTcVCRRqRSxHmPlAlDgnnalsReACKqqr', 'Gabriel', 'Thiel', 'user', '2023-01-16 08:55:15.52433', '2023-01-16 08:55:15.52433');
INSERT INTO public.users VALUES (64, 'JsuJaHx', 'AtAChaYCaQVRAhmyIFakUHjfpoSZHQdQZxssmEusSBdLcTXYkI', 'Jaunita', 'Haley', 'user', '2023-01-16 08:55:15.524545', '2023-01-16 08:55:15.524545');
INSERT INTO public.users VALUES (65, 'OLkUFML', 'XghoJrvqnZTDvhYScFfUQLpOsvGlsasvAatVIuaALEZlMYocfm', 'Whitney', 'Cassin', 'user', '2023-01-16 08:55:15.524763', '2023-01-16 08:55:15.524763');
INSERT INTO public.users VALUES (66, 'DQSBSYo', 'PvblqEkdBapMCckiayhKCYQaJliqWPfUUKnVoutVBqdJXINXxv', 'Paxton', 'Witting', 'user', '2023-01-16 08:55:15.52496', '2023-01-16 08:55:15.52496');
INSERT INTO public.users VALUES (67, 'oIbIZub', 'NtDXWjMhYEbttgkpyVnRdewRjMUBLNYvBgbScRbUpMsLkbNETB', 'Keanu', 'Zemlak', 'user', '2023-01-16 08:55:15.52515', '2023-01-16 08:55:15.52515');
INSERT INTO public.users VALUES (68, 'umvrqXt', 'SAPUkeVRQKdDJWshgObquXfFMIlvliJhVwAlXSwmPrQPRKvsNb', 'Orrin', 'Witting', 'user', '2023-01-16 08:55:15.525354', '2023-01-16 08:55:15.525354');
INSERT INTO public.users VALUES (69, 'RWRNDsg', 'CDvmoWSqmukxAJwsyeNSrPNoqDdufsHdHygcLHvGnPNfGBkeXV', 'Elena', 'Haag', 'user', '2023-01-16 08:55:15.525542', '2023-01-16 08:55:15.525542');
INSERT INTO public.users VALUES (70, 'FuLOsfD', 'hEoyNvWWZjuhVYfxtqPOkjxiNbgAmKnXdHLucQmukRCigdWDig', 'Dudley', 'Ruecker', 'user', '2023-01-16 08:55:15.525733', '2023-01-16 08:55:15.525733');
INSERT INTO public.users VALUES (71, 'cFGECnS', 'wiXBDuSxFyrBxoosZxWVKLJwXbRcTSrlDNNxHTEFUVlIcGWiDc', 'Aurelio', 'Block', 'user', '2023-01-16 08:55:15.525931', '2023-01-16 08:55:15.525931');
INSERT INTO public.users VALUES (72, 'DOANasK', 'UHiggGVeqMonOsbntTmVvbsMMTboxThiGAhpoHiTuCsmHlHAod', 'Korbin', 'Boehm', 'user', '2023-01-16 08:55:15.526121', '2023-01-16 08:55:15.526121');
INSERT INTO public.users VALUES (73, 'wDGgsWh', 'nohYtdnkQumfRunjIyUOjULIUIPYrIlvyyUtKjFoNbIflWfOoP', 'Darius', 'Bergstrom', 'user', '2023-01-16 08:55:15.526313', '2023-01-16 08:55:15.526313');
INSERT INTO public.users VALUES (74, 'CZBmTuS', 'QtgZJieQeDEfgnmwAjurPvDIfGPsQRiJOHWOMhJXqxncejqasJ', 'Omer', 'Reinger', 'user', '2023-01-16 08:55:15.526515', '2023-01-16 08:55:15.526515');
INSERT INTO public.users VALUES (75, 'WGfwFDW', 'TgQhewOLQmxjwvycTVXJUEsFMEsBUPFkCKetxqwxbKtbFrhMxt', 'Kelton', 'Bosco', 'user', '2023-01-16 08:55:15.526708', '2023-01-16 08:55:15.526708');
INSERT INTO public.users VALUES (76, 'RXbOaYZ', 'dEgMSUpbxCZINdkiPxQKEduUkNcMrXkPvRDWTFiKHuiKuMPAoS', 'Emely', 'Kris', 'user', '2023-01-16 08:55:15.526892', '2023-01-16 08:55:15.526892');
INSERT INTO public.users VALUES (77, 'FWuPKaM', 'vYSbcISuALHiVgdfcZogNKrjsiukRPnLRburZOrwUDcOPFhJrV', 'Ardella', 'McClure', 'user', '2023-01-16 08:55:15.527159', '2023-01-16 08:55:15.527159');
INSERT INTO public.users VALUES (78, 'ldwOJrp', 'QUntCPcwJHusZHVePAcNLpluwsuPFBmmQYvvRqPWlHMAvRwRnL', 'Edmond', 'Sipes', 'user', '2023-01-16 08:55:15.527422', '2023-01-16 08:55:15.527422');
INSERT INTO public.users VALUES (79, 'smvOjRN', 'TfTBhfTaKbQuNyAkKvpuRhFewfOZoVJACOiijPTepqErcftJmf', 'Carole', 'Gaylord', 'user', '2023-01-16 08:55:15.527644', '2023-01-16 08:55:15.527644');
INSERT INTO public.users VALUES (80, 'ghtJXKd', 'LCcKyjPxUXURcWJZmlNRmrNxplPZTRwtnSrCdBBjDTLKagylaU', 'Lorenza', 'Kemmer', 'user', '2023-01-16 08:55:15.527848', '2023-01-16 08:55:15.527848');
INSERT INTO public.users VALUES (81, 'JCMwZWo', 'mHJHSHfWgXGlrgmRlXfXMdlYnOiGwARHXJyUWSjNPhxTyYItiD', 'Alf', 'Bailey', 'user', '2023-01-16 08:55:15.528042', '2023-01-16 08:55:15.528042');
INSERT INTO public.users VALUES (82, 'utqxUSW', 'VBJSocWdxhwECZhvZKwrVYgxIlEQahjkAPlEBHHsQFYpPVrPft', 'Arturo', 'Ruecker', 'user', '2023-01-16 08:55:15.528227', '2023-01-16 08:55:15.528227');
INSERT INTO public.users VALUES (83, 'ZVsCvSJ', 'EWJnkRhSxPtsKLdlfwYqwZnOgNJPCtxtYAvQLdajitsObEHsqC', 'Berenice', 'Pfeffer', 'user', '2023-01-16 08:55:15.528424', '2023-01-16 08:55:15.528424');
INSERT INTO public.users VALUES (84, 'gSEgwiq', 'QxURwVMKYtyJrjFXQgpTioKgJDkkVCOUwhKORrRbmqOZSiqrxj', 'Golden', 'Mitchell', 'user', '2023-01-16 08:55:15.528609', '2023-01-16 08:55:15.528609');
INSERT INTO public.users VALUES (85, 'IVIgkFW', 'jgRjGKVOkvlkxIsMbZnXVUgSwyLfjAvGPCSvCgAaVNwbJDtWNT', 'Lawson', 'Daugherty', 'user', '2023-01-16 08:55:15.528796', '2023-01-16 08:55:15.528796');
INSERT INTO public.users VALUES (86, 'VHXcYJB', 'vdYplxlEroPFBEZXdqgIjoWFJJOPWYHDPJgBMVfSNnLMauBiJn', 'Luna', 'Abbott', 'user', '2023-01-16 08:55:15.529039', '2023-01-16 08:55:15.529039');
INSERT INTO public.users VALUES (87, 'udecqhv', 'xWwCFlUYXErCebpHESLKKCUkSfmGpnJmwcMCtsevogXIoYtekr', 'Brain', 'Morar', 'user', '2023-01-16 08:55:15.529297', '2023-01-16 08:55:15.529297');
INSERT INTO public.users VALUES (88, 'iWUTdkT', 'kDoAkGBgaXrPqKjyiHLPpfboBVUJJwOsTLpSEQbufrlvurBpjV', 'Duncan', 'Upton', 'user', '2023-01-16 08:55:15.529508', '2023-01-16 08:55:15.529508');
INSERT INTO public.users VALUES (89, 'iINowZp', 'FxWHXbtHKYVgxGDZqmKQdANFHigxRPAhInhHuUmBqgUNEqUAtA', 'Sheldon', 'Kemmer', 'user', '2023-01-16 08:55:15.529736', '2023-01-16 08:55:15.529736');
INSERT INTO public.users VALUES (90, 'BWMLxYI', 'cGlcgwxNqDxfHjikPBGxbTlObkvEnalRuvOOnjlMaNGfrMVrBd', 'Keagan', 'Kohler', 'user', '2023-01-16 08:55:15.529929', '2023-01-16 08:55:15.529929');
INSERT INTO public.users VALUES (91, 'NTRcYsI', 'dHnRnZgHgtuLSRPHVTUjkprtOqNCijyIZMsSgUsqWQAydlBRQe', 'Elouise', 'Effertz', 'user', '2023-01-16 08:55:15.53012', '2023-01-16 08:55:15.53012');
INSERT INTO public.users VALUES (92, 'rkmXxIv', 'xnkJhtcdSpwsvPNpJshqFIgoOOfMRVDDHJeRcPNiEmLxtCcjwU', 'Nicolas', 'Zulauf', 'user', '2023-01-16 08:55:15.530318', '2023-01-16 08:55:15.530318');
INSERT INTO public.users VALUES (93, 'yKdsRWx', 'mITbtCxKFBKWBSlkbLbElKqSbZAKPxKwnrIPyyqQhYcoyJDEsi', 'Earlene', 'Mante', 'user', '2023-01-16 08:55:15.53051', '2023-01-16 08:55:15.53051');
INSERT INTO public.users VALUES (94, 'UDktMMR', 'yUhybVGoIuKguTJZQUqpsoaaZrNMkGReAgrBCXwOXKvBSJEUTR', 'Rickey', 'Senger', 'user', '2023-01-16 08:55:15.530698', '2023-01-16 08:55:15.530698');
INSERT INTO public.users VALUES (95, 'bdUSGpY', 'eXggLdCrEXVhNtLGjRpDlqIxWRdkeEPAQrUMPgOqQeKoFAlgol', 'Hellen', 'Champlin', 'user', '2023-01-16 08:55:15.530896', '2023-01-16 08:55:15.530896');
INSERT INTO public.users VALUES (96, 'SpWuaYa', 'TsVJENwgiqBSqXhItoCdZxHRWDmwrkYWuBgBHwZYmUKadoTKlg', 'Sharon', 'Klocko', 'user', '2023-01-16 08:55:15.53109', '2023-01-16 08:55:15.53109');
INSERT INTO public.users VALUES (97, 'XWBHmyw', 'hDokhpOqYJMVwmKbbvNhPhMjLLSvGqXBDRjcWcklQLvpQusUrF', 'Era', 'Beatty', 'user', '2023-01-16 08:55:15.53128', '2023-01-16 08:55:15.53128');
INSERT INTO public.users VALUES (99, 'yVCbDXX', 'ILjAPrjLsRVAxVrIBmmxJERHdAbfQwQNQRbTBJcOiOHjVJOwDU', 'Elza', 'Hackett', 'user', '2023-01-16 08:55:15.531659', '2023-01-16 08:55:15.531659');
INSERT INTO public.users VALUES (100, 'ehgrilh', 'pTxNlKXlZInYQJRAtXsHkjadndmdcFlxUGPbfOhknIUNmpJtvy', 'Willa', 'Deckow', 'user', '2023-01-16 08:55:15.531848', '2023-01-16 08:55:15.531848');
INSERT INTO public.users VALUES (101, 'AbApbui', 'cCrPifEZjQSSZPQaUHMjKRnhiHcFtTJaJxBMsogFaSUnaSmOge', 'Terry', 'Roberts', 'user', '2023-01-16 08:55:15.532105', '2023-01-16 08:55:15.532105');
INSERT INTO public.users VALUES (102, 'yerVUhY', 'allLGyYOnQOhSlqaHaTAwhjLBGhYNYtbmZEQcbfVKAyggRaame', 'Wilburn', 'Ankunding', 'user', '2023-01-16 08:55:15.532363', '2023-01-16 08:55:15.532363');
INSERT INTO public.users VALUES (103, 'YtqDqoN', 'hLfEFWNtmnBOLQWhPlDalBEIrsJKOdRYcdAEsGkCRbZHoIvWyO', 'Hope', 'Mertz', 'user', '2023-01-16 08:55:15.532579', '2023-01-16 08:55:15.532579');
INSERT INTO public.users VALUES (104, 'pkKMNSB', 'pIgTxMYZrEwxDKVpwMxVCtiRoUgOCgaXdFcKaigAnwEVVJvNCm', 'Lennie', 'Bernier', 'user', '2023-01-16 08:55:15.532794', '2023-01-16 08:55:15.532794');
INSERT INTO public.users VALUES (105, 'VwKvLvE', 'vwfygHJFGvJvQihiHCdAmijZHGkDeKAnspaaGeVHGDDFtdXIqp', 'Makenzie', 'Jerde', 'user', '2023-01-16 08:55:15.53299', '2023-01-16 08:55:15.53299');
INSERT INTO public.users VALUES (106, 'ktIjsov', 'BpBtDuKKfcruqxKWXUqLGonYNNwbQJnuoRWZxTDrtbrUXpRpIr', 'Vidal', 'Lubowitz', 'user', '2023-01-16 08:55:15.53318', '2023-01-16 08:55:15.53318');
INSERT INTO public.users VALUES (107, 'uZQYLBF', 'wLjdQHpappJcRsTjGroWaLOnVMNXcaAtuLVPRAhQLCGflEgceb', 'Dominic', 'DuBuque', 'user', '2023-01-16 08:55:15.533375', '2023-01-16 08:55:15.533375');
INSERT INTO public.users VALUES (108, 'AoOCvFZ', 'EcmgVIUELqqTDZfsgoQrNXBacITrIeHPSuTMJvbPZUdZfNIUqX', 'Woodrow', 'Grady', 'user', '2023-01-16 08:55:15.533567', '2023-01-16 08:55:15.533567');
INSERT INTO public.users VALUES (109, 'QOVdmWU', 'EtHHxiutGdUCsgirEuMQGsrdfQtfJlWenDWUECaJRwVfvfEmvg', 'Jonas', 'Quigley', 'user', '2023-01-16 08:55:15.533753', '2023-01-16 08:55:15.533753');
INSERT INTO public.users VALUES (110, 'tAOckwC', 'JRBHhcAXuFAjfjkvoaZAGljODFNyLIwMWrqGABsnbdwdFfmpnp', 'Lavada', 'Brekke', 'user', '2023-01-16 08:55:15.533995', '2023-01-16 08:55:15.533995');
INSERT INTO public.users VALUES (111, 'usertest', 'test1', 'andrei', 'yurchenko', 'user', '2023-01-16 12:21:38.781498', '2023-01-16 12:26:39.372569');
INSERT INTO public.users VALUES (24, 'HoObYvi', '1234567890', 'Graham', 'Fritsch', 'administrator', '2023-01-16 08:55:15.515041', '2023-01-16 12:30:55.966884');


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: argolanguser
--

SELECT pg_catalog.setval('public.users_id_seq', 111, true);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: argolanguser
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: argolanguser
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- PostgreSQL database dump complete
--

