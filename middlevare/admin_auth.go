package middlevare

import (
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
)

// func AuthOwnerMiddleware(ctx *gin.Context) {
// 	token := ctx.Request.Header["Authorization"]
// 	if token[0] == "" {
// 		err := fmt.Errorf("missing authorization header")
// 		ctx.JSON(http.StatusUnauthorized, err)
// 		return
// 	}

// 	authJwt, err := ParseToken(ctx, token[0], jwtSecretSignature)
// 	if err != nil {
// 		ctx.JSON(http.StatusBadRequest, err)
// 	}

// 	if authJwt == nil {
// 		err := fmt.Errorf("smth went wrond idk what to write")
// 		ctx.JSON(http.StatusBadRequest, err)
// 		return
// 	}

// 	if authJwt.UserRole > 3 {
// 		ctx.JSON(http.StatusForbidden, gin.H{"error": "недостаточно прав для совершения данного действия"})
// 		return
// 	}

// 	ctx.Next()
// }

func AdminAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenCookie, err := r.Cookie("token")
		if err != nil {
			logrus.Errorf("error getting token: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Войдите сначала в аккаунт."))
			return
		}
		if tokenCookie == nil {
			logrus.Errorf("token cookie is nil")
			return
		}
		tokenStr := tokenCookie.Value

		authJwt, err := ParseToken(tokenStr, os.Getenv("JWT_SECRET"))
		if err != nil {
			logrus.Errorf("error parsing jwt token error: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Войдите сначала в аккаунт."))
			return
		}

		if authJwt == nil {
			logrus.Errorf("token is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Войдите сначала в аккаунт."))
			return
		}

		if authJwt.UserRole != "administrator" {
			logrus.Errorf("acces denied. Not enough rules")
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Недостаточно прав для выполнения данного действия."))
			return
		}
		next.ServeHTTP(w, r)
	})
}
