package middlevare

import (
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
)

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenCookie, err := r.Cookie("token")
		if err != nil {
			logrus.Errorf("error getting token: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Отсутствует токе авторизации. Пожалуйста, войдите в аккаунт"))
			return
		}
		if tokenCookie == nil {
			logrus.Errorf("token cookie is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Сначала выполните вход в аккаунт."))
			return
		}
		tokenStr := tokenCookie.Value

		authJwt, err := ParseToken(tokenStr, os.Getenv("JWT_SECRET"))
		if err != nil {
			logrus.Errorf("error parsing jwt token error: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Выполните вход в аккаунт"))
			return
		}

		if authJwt == nil {
			logrus.Errorf("token is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Выполните вход в аккаунт."))
			return
		}
		next.ServeHTTP(w, r)
	})
}
