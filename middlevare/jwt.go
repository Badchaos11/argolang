package middlevare

import (
	"context"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const jwtSecretSignature = `8Zz5tw0Ionm3XPZZfN0NOml3z9FMfmpgXwovR9fp6ryDIoGRM8EPHAB6iHsc0fb`

type Jwt struct {
	Username string `json:"username"`
	UserRole string `json:"user_role"`
	jwt.StandardClaims
}

func NewJwt(ctx context.Context, username string, userRole string) (*Jwt, error) {
	return &Jwt{
		Username: username,
		UserRole: userRole,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
		},
	}, nil
}

func NewToken(ctx context.Context, username string, userRole string) (string, error) {
	auth, err := NewJwt(ctx, username, userRole)
	if err != nil {
		return "", err
	}

	return getTokenString(auth)
}

func getTokenString(auth *Jwt) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, auth)
	tokenString, err := token.SignedString([]byte(jwtSecretSignature))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func ParseToken(token, secret string) (*Jwt, error) {
	jwtToken, err := jwt.ParseWithClaims(token, &Jwt{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("invalid signing method")
		}

		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}

	if jwtToken == nil || jwtToken.Claims == nil {
		return nil, fmt.Errorf("token or claims is null")
	}

	authJwt, ok := jwtToken.Claims.(*Jwt)
	if !ok || !jwtToken.Valid {
		return nil, fmt.Errorf("not valid token")
	}

	return authJwt, nil

}
