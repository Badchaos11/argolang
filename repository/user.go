package repository

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/model"
)

func (r *PGRepository) CreateUser(ctx context.Context, user model.NewUser) (int64, error) {
	const query = `INSERT INTO users (username, password, name, surname, role, created, updated) VALUES ($1, $2, $3, $4, 'user', now(), now()) RETURNING id;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var id int64

	err := r.MainConnection.QueryRowContext(ctx, query, user.Username, user.Password, user.Name, user.Surname).Scan(&id)
	if err != nil {
		logrus.Errorf("Error querying user error: %v", err)
		return 0, err
	}

	return id, nil
}

func (r *PGRepository) GetUserForLogin(ctx context.Context, user model.LoginRequest) (string, error) {
	const query = `SELECT role FROM users WHERE username=$1 AND password=$2;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var role string

	err := r.MainConnection.QueryRowContext(ctx, query, user.Login, user.Password).Scan(&role)
	if err != nil {
		logrus.Errorf("error querying user error %v", err)
		return "", err
	}

	return role, nil
}

func (r *PGRepository) GetUserByID(ctx context.Context, userId int64) (*model.ResponseUser, error) {
	const query = `SELECT username, name, surname, role, created FROM users WHERE id=$1;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var out model.ResponseUser

	err := r.MainConnection.QueryRowContext(ctx, query, userId).Scan(&out.Username, &out.Name, &out.Surname, &out.Role, &out.Created)
	if err != nil {
		logrus.Errorf("error quering user %v", err)
		return nil, err
	}

	return &out, err
}

func (r *PGRepository) ChangePassword(ctx context.Context, password, username string) error {
	const query = `UPDATE users SET password=$2, updated=now() WHERE username=$1;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MainConnection.ExecContext(ctx, query, username, password)
	if err != nil {
		logrus.Errorf("error updating user password: %v", err)
		return err
	}

	return nil
}
