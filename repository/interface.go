package repository

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/model"
)

type IRepository interface {
	// Общий функционал
	CreateUser(ctx context.Context, newUser model.NewUser) (int64, error)
	GetUserByID(ctx context.Context, userId int64) (*model.ResponseUser, error)
	GetUserForLogin(ctx context.Context, user model.LoginRequest) (string, error)
	ChangePassword(ctx context.Context, password, username string) error
	// Функционал админа
	GetFullUserDataByID(ctx context.Context, userId int64) (*model.User, error)
	DeleteUser(ctx context.Context, userId int64) error
	ChangeUserRole(ctx context.Context, userId int64, newRole string) error
	ChangePasswordByID(ctx context.Context, userId int64, newPassword string) error
}

type PGRepository struct {
	MainConnection *sqlx.DB
	timeout        time.Duration
}

func NewRepository(dsn string) (IRepository, error) {
	conn, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		logrus.Errorf("error connect to database err: %v", err)
		return nil, err
	}

	return &PGRepository{
		MainConnection: conn,
		timeout:        time.Second * 5,
	}, nil
}
