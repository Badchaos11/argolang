package repository

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/argolang/model"
)

func (r *PGRepository) GetFullUserDataByID(ctx context.Context, userId int64) (*model.User, error) {
	const query = `SELECT * FROM users WHERE id=$1`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var out model.User

	err := r.MainConnection.QueryRowContext(ctx, query, userId).Scan(&out.ID, &out.Username, &out.Password, &out.Name, &out.Surname, &out.Role, &out.Created, &out.Updated)
	if err != nil {
		logrus.Errorf("error querying user %v", err)
		return nil, err
	}

	return &out, nil
}

func (r *PGRepository) DeleteUser(ctx context.Context, userId int64) error {
	const query = `DELETE FROM users WHERE id=$1`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MainConnection.ExecContext(ctx, query, userId)
	if err != nil {
		logrus.Errorf("error deleting user %v", err)
		return err
	}

	return nil
}

func (r *PGRepository) ChangeUserRole(ctx context.Context, userId int64, newRole string) error {
	const query = `UPDATE users SET role=$2, updated=now() WHERE id=$1;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MainConnection.ExecContext(ctx, query, userId, newRole)
	if err != nil {
		logrus.Errorf("error updating user role %v", err)
		return err
	}

	return nil
}

func (r *PGRepository) ChangePasswordByID(ctx context.Context, userId int64, newPassword string) error {
	const query = `UPDATE users SET password=$2, updated=now() WHERE id=$1;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MainConnection.ExecContext(ctx, query, userId, newPassword)
	if err != nil {
		logrus.Errorf("error updating password %v", err)
		return err
	}

	return nil
}
